import Vue from 'vue'
import Router from 'vue-router'
const Dashboard = () => import ('./views/Dashboard.vue')
const About = () => import ('./views/About.vue')
const Email = () => import ('./components/Email.vue')
const WidgetData = () => import ('./components/WidgetData.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      children: [
        {
          path: '/email',
          component: Email
        },
        {
          path: '/widget-data',
          component: WidgetData
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      component: About
    }
  ]
})
