import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	sidebar: {
  		visible: true,
      background: ''
  	},
    navbar: {
      background: '',
      logoBackground: ''
    }
  },
  mutations: {
  	toggleSidebar(state){
  		state.sidebar.visible = ! state.sidebar.visible
  	},
    changeLogoBackground(state , color){
      state.navbar.logoBackground = color
    },
    changeNavbarBackground(state , color){
      state.navbar.background = color
    },
    changeSidebarBackground(state , color){
      state.sidebar.background = color
    }
  },
  actions: {

  }
})
