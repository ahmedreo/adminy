import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Libraries
import ElementUI  from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import "@/assets/sass/main.scss";
import "animate.css/animate.min.css";
import Vuebar from 'vuebar';
import * as vClickOutside from 'v-click-outside-x';

Vue.use(ElementUI)
Vue.use(Vuebar)
Vue.use(vClickOutside);

// Directives , Plugins and Filters
require('./directives.js')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
