module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "src/assets/sass/shared/package.scss";
          @import "src/assets/sass/shared/animation.scss";
        `
      }
    }
  }
}